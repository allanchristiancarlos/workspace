# Workspace


## Requirements
- Vagrant [download](https://www.vagrantup.com/downloads.html)
- Virtual box [download](https://www.virtualbox.org/wiki/Downloads)
- Vagrant hosts update
- workspace.box  kunin niyo kay allan.
- git [download](https://git-scm.com/downloads)

## Installation
- Install git
- Install vagrant
- Install virtual box
- run `vagrant plugin install vagrant-hostsupdater` in command line
- Install allan/workspace.box by going to command line `vagrant box add allan/workspace path/of/workspace.box`
- open terminal or command line and change the path using `cd folder_name_where_to_install_workspace`
- clone this repo `git clone https://bitbucket.org/allanchristiancarlos/workspace.git`
- change directory `cd workspace`
- code `vagrant up --provision`
- done access `workspace.dev` in browser